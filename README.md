# n002-math.sty

This repository contains `n002-math.sty`, a LaTeX style file which requires
some useful math-related packages and provides some useful math-related
macros.

It is intended to be used with [`n002-things.sty`][n002-things], a style file
for general LaTeX projects.


[n002-things]: https://gitlab.com/aergus/n002-things

