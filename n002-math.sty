% n002-math.sty
%
% Written in 2015-2017 by Aras Ergus <arasergus@posteo.net>.
%
% This file is available for use under the terms of CC0, in particular in
% public domain where possible. For more information, please see:
% http://creativecommons.org/publicdomain/zero/1.0/

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{n002-math}[2017/05/23]

% Commonly used math-related packages.

\RequirePackage{amsfonts}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{dsfont}
\RequirePackage{faktor}
\RequirePackage{mathabx}
\RequirePackage{mathtools}
\RequirePackage{stmaryrd}
\RequirePackage{tikz-cd}

\usetikzlibrary{shapes.geometric}

% Theorems et al. are numbered within sections.

\RequirePackage{amsthm}
\RequirePackage{thmtools}

\declaretheorem[
  name = Theorem,
  numberwithin = section
]{theorem}
\declaretheorem[
  name = Corollary,
  sibling = theorem
]{corollary}
\declaretheorem[
  name = Lemma,
  sibling = theorem
]{lemma}
\declaretheorem[
  name = Proposition,
  sibling = theorem
]{proposition}
\declaretheorem[
  name = Fact,
  sibling = theorem
]{fact}

\declaretheorem[
  name = Remark,
  sibling = theorem,
  style=definition
]{remark}
\declaretheorem[
  name = Example,
  sibling = theorem,
  style=definition
]{example}
\declaretheorem[
  name = Definition,
  sibling = theorem,
  style=definition
]{definition}
\declaretheorem[
  name = Notation,
  sibling = theorem,
  style=definition
]{notation}
\declaretheorem[
  name = Convention,
  sibling = theorem,
  style=definition
]{convention}
\declaretheorem[
  name = Construction,
  sibling = theorem,
  style = definition
]{construction}

% Shortcuts for some mathematical symbols.

\RequirePackage{ifthen}
\RequirePackage[cal=boondoxo]{mathalfa}

% Some number "domain"s.
\newcommand{\CC}{\mathds{C}}
\newcommand{\NN}{\mathds{N}}
\newcommand{\QQ}{\mathds{Q}}
\newcommand{\RR}{\mathds{R}}
\newcommand{\ZZ}{\mathds{Z}}

% Some algebras.
\newcommand{\polynomials}[2]{#1\left[ #2 \right]}
\newcommand{\powerseries}[2]{#1\left\llbracket #2 \right\rrbracket}

% Working with functions.
\newcommand{\dom}{\operatorname{dom}}
\newcommand{\ran}{\operatorname{ran}}
\newcommand{\rk}{\operatorname{rk}}
\newcommand{\id}{\mathrm{id}}
\newcommand{\inc}{\mathrm{in}}
\newcommand{\pro}{\mathrm{pr}}

% Linear groups.
\newcommand{\lineargroup}[3]{\operatorname{#1}_{#2}(#3)}
\newcommand{\GL}[2]{\lineargroup{GL}{#1}{#2}}
\newcommand{\PGL}[2]{\lineargroup{PGL}{#1}{#2}}
\newcommand{\SL}[2]{\lineargroup{SL}{#1}{#2}}
\newcommand{\PSL}[2]{\lineargroup{PSL}{#1}{#2}}
\newcommand{\Ort}[1]{\operatorname{O}(#1)}
\newcommand{\Uni}[1]{\operatorname{U}(#1)}

% Hom'ies.
\newcommand{\subscriptif}[1]{\ifthenelse{\equal{#1}{}}{}{_{#1}}\!}
\newcommand{\Hom}[3][]{
  \operatorname{Hom}\subscriptif{#1}\left({#2},{#3}\right)
}
\newcommand{\End}[2][]{
  \operatorname{End}\subscriptif{#1}\left({#2}\right)
}
\newcommand{\Aut}[2][]{
  \operatorname{Aut}\subscriptif{#1}\left({#2}\right)
}
\newcommand{\IHom}[3][]{
  \operatorname{\underline{Hom}}\subscriptif{#1}\left({#2},{#3}\right)
}
\newcommand{\IEnd}[2][]{
  \operatorname{\underline{End}}\subscriptif{#1}\left({#2}\right)
}
\newcommand{\IAut}[2][]{
  \operatorname{\underline{Aut}}\subscriptif{#1}\left({#2}\right)
}
\newcommand{\CHom}[3][]{
  \operatorname{\mathcal{H\!o\!m}}\subscriptif{#1}\left({#2},{#3}\right)
}
\newcommand{\CEnd}[2][]{
  \operatorname{\mathcal{E\!n\!d}}\subscriptif{#1}\left({#2}\right)
}
\newcommand{\CAut}[2][]{
  \operatorname{\mathcal{A\!u\!t}}\subscriptif{#1}\left({#2}\right)
}

% variable blanks
\newcommand{\blnkn}[1]{
  \hspace{0.15ex}
  \begin{tikzpicture}
  \node[
    regular polygon,
    regular polygon sides=#1,
    anchor=north,
    fill=black,
    style={scale=0.6}
  ] at (0,0) {};
  \end{tikzpicture}
  \hspace{0.15ex}
}
\newcommand{\blnktria}{
    \hspace{0.15ex}
    \begin{tikzpicture}
    \node[
    regular polygon,
    regular polygon sides=3,
    anchor=north,
    fill=black,
    style={scale=0.4}
    ] at (0,0) {};
    \end{tikzpicture}
    \hspace{0.15ex}
}
\newcommand{\blnksq}{\blnkn{4}}
\newcommand{\blnkpenta}{\blnkn{5}}
\newcommand{\blnkhexa}{\blnkn{6}}
\newcommand{\blnkhepta}{\blnkn{7}}
\newcommand{\blnk}{\underline{\ \,}}


% Some cats!
\newcommand{\ob}{\operatorname{ob}}
\newcommand{\mor}{\operatorname{mor}}
\newif\ifcurlycat
\curlycatfalse
\newcommand{\curlycat}{\curlycattrue}
\newcommand{\catstyle}[1]{
  \ifcurlycat
    \mathcal{#1}
  \else
    \mathbf{#1}
  \fi
}
\newcommand{\Set}{\catstyle{Set}}
\newcommand{\Mon}{\catstyle{Mon}}
\newcommand{\Grp}{\catstyle{Grp}}
\newcommand{\Ab}{\catstyle{Ab}}
\newcommand{\AbMon}{\catstyle{AbMon}}
\newcommand{\Cat}{\catstyle{Cat}}
\newcommand{\CAT}{\catstyle{CAT}}
\newcommand{\SimplexCat}{\mathbf{\Delta}}
\newcommand{\Fin}{\catstyle{Fin}}
\newcommand{\Ch}{\operatorname{Ch}}
\newcommand{\DerCat}{\mathcal{D}}
\newcommand{\op}[1]{{#1}^\mathrm{op}}
\newcommand{\algebraobjectsin}[2]{\operatorname{#1-#2}}

% Some people like to use Ker for kernels.
\newif\ifuppercaseker
\uppercasekerfalse
\newcommand{\uppercaseker}{\uppercasekertrue}
\renewcommand{\ker}{
  \ifuppercaseker
    \operatorname{Ker}
  \else
    \operatorname{ker}
  \fi
}
\newcommand{\im}{
  \ifuppercaseker
    \operatorname{Im}
  \else
    \operatorname{im}
  \fi
}
\newcommand{\essim}{\operatorname{essim}}

% "Braket"s.
\newcommand{\braket}[1]{\left\langle #1 \right\rangle}

% Class notations.
\newcommand{\setbrack}[1]{\left\{ #1 \right\}}
\newcommand{\class}[2]{\setbrack{#1 \mid #2}}
\newcommand{\inlinesetbrack}[1]{\{ #1 \}}
\newcommand{\inlineclass}[2]{\inlinesetbrack{#1 \mid #2}}

% Some logic / set theory symbols in case someone needs them.
\newcommand{\goed}[1]{\ulcorner {#1} \urcorner}
\newcommand{\ZF}{\mathrm{ZF}}
\newcommand{\ZFMinus}{\mathrm{ZF}^-}
\newcommand{\ZFC}{\mathrm{ZFC}}

\endinput
